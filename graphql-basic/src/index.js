import {GraphQLServer} from 'graphql-yoga'

// Type defination (Schema)

const typeDefs = `
 type Query {
    greeting(name:String) : String!
    me: User!
    address : Address!
    add(a:Int!, b:Int!): Float
    scores: [Int!]!
    addArray(numbers:[Int!]): Float!
    getPosts(query:Int):[Post!]!
 }
 type User {
     id:ID!
     name:String!
     email:String!
     age:Int!
     gpa:Float
     address: Address
 }
 type Address {
     landmark: String!
     city: String!
     zipcode: Int!
     state: String
 }
 type Post {
     id: ID!
     title: String!
     description: String!
     posted: Boolean!
     likedBy: Int!
 }
`
const posts =[{
    'id':1,
    'title': 'Good tutorial',
    'description':'dsnfnsd sdfjksdf sdfsdf',
    'posted': true,
    'likedBy': 11
},{
    'id':2,
    'title': 'Not Good tutorial',
    'description':'dsnfnsd sdfjksdf sdfsdf',
    'posted': true,
    'likedBy': 5
},{
    'id':3,
    'title': 'Good tutorial',
    'description':'dsnfnsd sdfjksdf sdfsdf',
    'posted': false,
    'likedBy': 0
}]

const resolvers ={
    Query :{
       me(){
           return {
               id:'1234',
               name:'AMol',
               email:'amol.s@servify.tech',
               age:27,
               gpa:7.01,
               address :{
                   landmark:'Near hanuman temple',
                   city:'Aurangabad',
                   zipcode:423702,
                   state:'maharashtra'
               }
           }
       },
       address(){
           return {
            landmark:'Near hanuman temple',
            city:'Aurangabad',
            zipcode:423702,
            state:'maharashtra'
        }
       },
       greeting(parent,args,ctx,info){
        if(args.name){
            return `Welcome ${args.name}`;
        }else{
            return `Welcome`
        }
       },
       add(parent,args,ctx,info){
           console.log(args);
        if(args.a && args.b){
            return args.a + args.b;
        }else{
            0
        }
       },
       scores(parent,args,ctx,info){
           return [25,59,53];
       },
       addArray(parent,args,ctx,info){
           if(args.numbers.length){
           return args.numbers.reduce((cumulative,currentvalue)=>{
                return cumulative + currentvalue;
            })
           }else{
               return 0
           }
       },
       getPosts(parent,args,ctx,info){
           if(args.query){
            return posts.filter((p)=>{
                return p.likedBy > args.query
            });
           }else{
               return posts;
           }
       }
    }
};

const server = new GraphQLServer({
    typeDefs,
    resolvers
});

let options = {
    port: 8000,
    endpoint: '/graphql',
    subscriptions: '/subscriptions',
    playground: '/playground',
  };

server.start(options,({port})=>{
    console.log(`GraphQl server started at default ${port}`);
});